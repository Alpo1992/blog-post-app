# Generated by Django 4.0.4 on 2022-05-31 02:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0002_rename_review_post'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='authors',
        ),
        migrations.RemoveField(
            model_name='post',
            name='cover_image',
        ),
        migrations.RemoveField(
            model_name='post',
            name='created',
        ),
        migrations.RemoveField(
            model_name='post',
            name='rating',
        ),
        migrations.RemoveField(
            model_name='post',
            name='summary',
        ),
    ]
